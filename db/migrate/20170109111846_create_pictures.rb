class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.string :image
      t.string :name
      t.references :event, foreign_key: true, index: true
      t.references :service, foreign_key: true, index: true

      t.timestamps
    end
  end
end
