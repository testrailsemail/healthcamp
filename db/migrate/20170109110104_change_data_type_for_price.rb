class ChangeDataTypeForPrice < ActiveRecord::Migration[5.0]
  def self.up
    change_column(:services, :price, :decimal, {precision: 8, scale: 2})

  end
  def self.down
    change_column(:services, :price, :decimal)
  end
end
