class AddOftypeToLinesArticles < ActiveRecord::Migration[5.0]
  def up
    add_column :lines_articles, :oftype, :string, default: "pages"
    Lines::Article.find_each do |article|
      article.oftype = 'pages'
      article.save!
    end
  end

  def down
    remove_column :lines_articles, :oftype
  end
end
