require 'rails_helper'

RSpec.describe "services/index", type: :view do
  before(:each) do
    assign(:services, [
      Service.create!(
        :name => "Name",
        :description => "Description",
        :price => 20
      ),
      Service.create!(
        :name => "Name",
        :description => "Description",
        :price => 20
      )
    ])
  end

  it "renders a list of services" do
    render
    assert_select ".desc>h3", :text => "Name".to_s, :count => 2
    assert_select ".desc>p", :text => "Description".to_s, :count => 2
    assert_select ".desc>span", :text => "20,00 грн.".to_s, :count => 2
  end
end
