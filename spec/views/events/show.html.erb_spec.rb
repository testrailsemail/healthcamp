require 'rails_helper'

RSpec.describe "events/show", type: :view do
  before(:each) do
    @item = create(:event)
    @event = assign(:event, @item)
  end

  it "renders attributes in <p>" do
    render
    assert_select ".each_event-show>h2", :text => @item.name.to_s
    assert_select ".each_event-show>p", :text => @item.description.to_s
    assert_select ".each_event-show>img[src=?]", @item.pic.image_url, :count => 1
  end
end
