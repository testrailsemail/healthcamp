require 'rails_helper'

RSpec.describe "events/index", type: :view do
  before(:each) do
    @item = create(:event)
    assign(:events, [
      @item,
      @item
    ])
  end

  it "renders a list of events" do
    render
    tim = @item.start.strftime("db:‘%Y-%m-%d %H:%M:%S’")
    assert_select ".excerpt>h6", :text => @item.name.to_s, :count => 2
    assert_select ".excerpt>p", :text => @item.description.to_s, :count => 2
    assert_select ".excerpt>time[datetime=?]", tim, :count => 2
  end
end
