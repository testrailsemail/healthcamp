module Pundit
  module Matchers
    RSpec::Matchers.define :permit do |action|
      match do |policy|
        policy.public_send("#{action}?")
      end

      failure_message do |policy|
        "#{policy.class} does not permit #{action} on #{policy.record} for #{policy.user.role}."
      end

      failure_message_when_negated do |policy|
        "#{policy.class} does not forbid #{action} on #{policy.record} for #{policy.user.role}."
      end
    end

    RSpec::Matchers.define :forbid do |action|
      match do |policy|
        !policy.public_send("#{action}?")
      end

      failure_message do |policy|
        "#{policy.class} does not forbid #{action} on #{policy.record} for #{policy.user.role}."
      end

      failure_message_when_negated do |policy|
        "#{policy.class} does not permit #{action} on #{policy.record} for #{policy.user.role}."
      end
    end
  end
end