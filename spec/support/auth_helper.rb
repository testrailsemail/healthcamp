module AuthHelper
  def http_login
    user = 'admin'
    pw = 'secret'
    request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials(user,pw)
  end

  def set_user_session(user)
    session[:user_id] = user.id
  end

  def sign_in(user)
    visit store_path
    click_link 'log in'
    fill_in 'Name:', with: user.name
    fill_in 'Password:', with: user.password
    click_button 'Login'
  end

end