FactoryGirl.define do
  factory :service do
    name "MyString"
    description "MyText"
    price "9.99"
  end
end
