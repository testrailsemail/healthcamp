FactoryGirl.define do
  factory :event do
    name {Faker::Lorem.sentence}
    description {Faker::Lorem.paragraph}
    start {Faker::Date.forward(2)}

    after(:create) do |event|
      FactoryGirl.create(:event_pic, event: event)
    end
  end
end
