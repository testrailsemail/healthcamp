FactoryGirl.define do
  factory :pic do
    event nil
    service nil

    image Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/public/healthcamp.jpg')))

    factory :event_pic do
      event :event
    end

    factory :service_pic do
      service :service
    end
  end
end
