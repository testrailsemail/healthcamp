require 'rails_helper'

RSpec.describe Event, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.build(:event)).to be_valid
  end

  it { should have_one :pic }

  describe "presence validations" do
    before :each do
      @event = Event.new(name: nil, description: nil, start: nil)
      @event.valid?
    end

    it "is invalid without a name " do 
      expect(@event.errors[:name]).to include(I18n.t('.errors.messages.blank'))
    end

    it "is invalid without a description" do 
      expect(@event.errors[:description]).to include(I18n.t('.errors.messages.blank'))
    end

  end

  context "uniqueness and other valid event checks" do

  end

  it "is valid with a name, description and start date" do
    event = Event.new(name: "My Book name", description: "Long description", 
                          start: "2020-02-18 09:00:00 UTC")
    expect(event).to be_valid
  end

  it "ensures it has date in the future" do
    event = Event.new(name: "My Book name", description: "Long description", 
                          start: "2016-02-18 09:00:00 UTC")
    event.save
    expect(event.errors[:start]).to include("can't be in the past")
  end

end
