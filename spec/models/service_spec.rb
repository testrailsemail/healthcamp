require 'rails_helper'

RSpec.describe Service, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.build(:service)).to be_valid
  end

  describe "presence validations" do
    before :each do
      @service = Service.new(name: nil, description: nil, price: nil)
      @service.valid?
    end

    it "is invalid without a name " do 
      expect(@service.errors[:name]).to include(I18n.t('.errors.messages.blank'))
    end

    it "is invalid without a description" do 
      expect(@service.errors[:description]).to include(I18n.t('.errors.messages.blank'))
    end

    it "is invalid without a price " do 
      expect(@service.errors[:price]).to include(I18n.t('.errors.messages.blank'))
    end

  end

  context "uniqueness and other valid service checks" do

  end

  it "is valid with a name, description and price" do
    service = Service.new(name: "My Book name", description: "Long description", 
                          price: 20)
    expect(service).to be_valid
  end

  it "ensures it has price greater than 0" do
    service = Service.new(name: "My Book name", description: "Long description", 
                          price: 0)
    service.save
    expect(service.errors[:price]).to include(I18n.t('.errors.messages.greater_than_or_equal_to', count: 0.01))
  end
end
