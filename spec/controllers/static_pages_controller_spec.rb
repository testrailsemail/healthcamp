require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

  # describe "GET #home" do
  #   it "returns http success" do
  #     get :home
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "GET #contacts" do
  #   it "returns http success" do
  #     get :contacts
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  describe "GET #project" do
    it "returns http success" do
      create(:article, title: "Фотографии Подобовец")
      get :project
      expect(response).to have_http_status(:success)
    end
  end

end
