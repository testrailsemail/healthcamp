# See http://www.robotstxt.org/robotstxt.html for documentation on how to use the robots.txt file
#
# To ban all spiders from the entire site uncomment the next two lines:
# User-agent: *
# Disallow: /
Sitemap: https://s3-eu-west-1.amazonaws.com/healthcamp/sitemaps/sitemap.xml.gz
