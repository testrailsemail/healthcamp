# Lists all Articles and shows specific ones.
# Responds either to HTML and ATOM requests.
#require_dependency "lines/application_controller"
require_dependency Lines::Engine.root.join('app', 'controllers', 'lines', 'admin', 'application_controller').to_s


class BlogsController < ApplicationController
  before_action :authorize, except: [:index, :show]
  layout 'lines/application'

  KEYWORDS = CONFIG[:keywords]
  SITE_TITLE = CONFIG[:title]

  # Lists all published articles.
  # Responds to html and atom
  def index
    respond_to do |format|
      format.html {
        @first_page = (params[:page] and params[:page].to_i > 0) ? false : true
        if params[:tag]
          @articles = Lines::Article.where(oftype: "blog").published.tagged_with(params[:tag]).page(params[:page].to_i)
        else
          @articles = Lines::Article.where(oftype: "blog").published.page(params[:page].to_i).padding(1)
        end
        
        if @articles.first_page?
          if @first_article = Lines::Article.where(oftype: "blog").published.first
            @first_article.teaser = nil unless @first_article.teaser.present?
          end
        end
        
        set_meta_tags title: SITE_TITLE,
                      description: CONFIG[:meta_description],
                      keywords: KEYWORDS,
                      open_graph: { title: SITE_TITLE,
                                      type: 'website',
                                      url: articles_url,
                                      site_name: SITE_TITLE,
                                      image: CONFIG[:og_logo]
                                    }

      render 'lines/articles/index'
      }
      format.atom{
        @articles = Lines::Article.where(oftype: "blog").published
        render 'lines/articles/index'
      }
    end
  end

  # Shows specific article
  def show
    @first_page = true
    @article = Lines::Article.where(oftype: "blog").published.find(params[:id])
    @article.teaser = nil unless @article.teaser.present?
    meta_tags = { title: @article.title,
      type: 'article',
      url: url_for(@article),
      site_name: SITE_TITLE,
    }
    meta_tags[:image] = CONFIG[:host] + @article.image_url if @article.image_url.present?
    set_meta_tags title: @article.title,
                  keywords: KEYWORDS + @article.tag_list.to_s,
                  open_graph: meta_tags
    if request.path != article_path(@article)
      return redirect_to @article, status: :moved_permanently
    end

    @related_articles = Lines::Article.where(oftype: "blog").where('id != ?', @article.id).published.order('').limit(2)

    render 'lines/articles/show'
  end

  def to_blog
    @article = Lines::Article.friendly.find(params[:id])
    if @article.update_attributes(oftype: "blog")
      redirect_to lines.admin_articles_path, :notice => "Article converted to Blog post."
    end
  end

  def to_pages
    @article = Lines::Article.friendly.find(params[:id])
    if @article.update_attributes(oftype: "pages")
      redirect_to lines.admin_articles_path, :notice => "Blog post converted to Article."
    end
  end

  private

    # sets the current_lines_user if one exists in session
    def current_lines_user
      @current_lines_user ||= Lines::User.find(session[:user_id]) if session[:user_id]
    end
    helper_method :current_lines_user

    # checks if current user is authorized. Redirects to login_url if not
    def authorize
      redirect_to lines.login_url, notice: "Please_login" if current_lines_user.nil?
    end

end