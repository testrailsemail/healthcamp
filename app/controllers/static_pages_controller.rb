class StaticPagesController < ApplicationController
  SITE_TITLE = "Healthcamp"

  # def home
  # end

  # def contacts
  # end

  def project
    set_meta_tags title: 'О проекте',
                  description: "Выездной лагерь: возможность потренироваться, пробежки, растяжка, 
                                миофасциальное расслабление, TRX. Диетология, 
                                спортивное питание и прочее.",
                  open_graph: { title: 'HealthCamp - Kyiv', 
                                type: 'website',
                                url: root_url,
                                site_name: SITE_TITLE,
                                image: 'http://healthcamp.com.ua/healthcamp.jpg'
                              }

    @content = Lines::Article.find('o-proekte')
    @coach1 = Lines::Article.find('oksana-tasanova')
    @coach2 = Lines::Article.find('yuliya-zaydenberg')
    fresh_when(@content && @coach1 && @coach2)
  end
end
