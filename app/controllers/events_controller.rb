class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  SITE_TITLE = "Healthcamp"
  # GET /events
  # GET /events.json
  def index

    set_meta_tags title: 'Мероприятия',
                  description: "Выездной лагерь: возможность потренироваться, пробежки, растяжка, 
                                миофасциальное расслабление, TRX, даже скакалка. Диетология, 
                                спортивное питание и прочее.",
                  open_graph: { title: 'HealthCamp - Kyiv', 
                                type: 'website',
                                url: root_url,
                                site_name: SITE_TITLE,
                                image: 'http://healthcamp.com.ua/healthcamp.jpg'
                              }
    @events = Event.where('start > ?', DateTime.now)
    fresh_when(@events)
  end

  # GET /events/1
  # GET /events/1.json
  def show
    meta_tags = { title: @event.name,
        type: 'website',
        url: url_for(@event)
      }
    meta_tags[:image] = @event.pic.image.url if @event.pic.present?
    set_meta_tags title: "#{@event.name}",
                  description: "Запланированное мероприятие: #{@event.description}",
                  open_graph: meta_tags
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :description, :start)
    end
end

