class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]

  SITE_TITLE = "Healthcamp"
  # GET /services
  # GET /services.json
  def index
    set_meta_tags title: 'Услуги',
                  description: "Предлагаемые услуги",
                  # author: "http://yourgplusprofile.com/profile/url"
                  open_graph: { title: 'HealthCamp - Kyiv, Услуги', 
                                type: 'website',
                                url: services_url,
                                site_name: SITE_TITLE,
                                image: 'http://healthcamp.com.ua/healthcamp.jpg'
                              }

    @services = Service.all
    fresh_when(@services)
  end

  # GET /services/1
  # GET /services/1.json
  def show
    meta_tags = { title: @service.name,
        type: 'website',
        url: url_for(@service)
    }
    meta_tags[:image] = @service.pic.image.url if @service.pic.present?
    
    set_meta_tags title: "#{@service.name}",
                  description: "Предлагаемая услуга: #{@service.description}",
                  open_graph: meta_tags
    fresh_when(@service)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:name, :description, :price)
    end
end
