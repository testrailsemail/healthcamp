ActiveAdmin.register Event do
menu priority: 6, label: "Мероприятия"

permit_params :name, :description, :start,
    pic_attributes: [:id, :name, :image, :event_id, :remote_image_url]

action_item :new, only: :show do
  link_to 'Новое мероприятие', new_admin_event_path
end

action_item :show, only: [:edit, :show] do
    link_to 'Посмотреть на сайте', event_path(event)
  end
# action_item :edit, only: :show do
#   link_to 'Отредактировать', edit_admin_event_path(event)
# end

scope "Запланированы", :active  do |event|
  events.where('start > ?', Time.now)
end
  index do
    selectable_column #batch actions checkboxes column
    actions
    column :id
    column "Название", :name
    column "Описание", :description
    column "Начало", :start
    column "Обновлено", :updated_at
    column "Создано", :created_at
  end
  
  show do
    attributes_table do
      row ("Название") { event.name }
      row ("Описание") { event.description }
      row ("Начало") { event.start }
      row ("Создано") { event.created_at }
      row ("Обновлено") { event.updated_at }
      row :pic do
        image_tag event.pic.image.url if event.pic
      end
    end
    active_admin_comments
  end


  form do |f|
    f.inputs 'Details' do
      f.input :name, label: 'Название'
      f.input :description, label: 'Описание', input_html: { rows: 5 }
      # f.input :start, label: 'When', input_html: { :value => DateTime.now }
      f.input :start, label: 'Дата', as: :datepicker, input_html: { value: f.object.start.nil? ? DateTime.now.try(:strftime, '%Y-%m-%d') : f.object.start.try(:strftime, '%Y-%m-%d'), style: 'width:6%' }
      f.input :start_time, label: 'Время', as: :time_picker, input_html: { value: f.object.start.nil? ? DateTime.now.try(:strftime, '%H:%M') : f.object.start.try(:strftime, '%H:%M'), style: 'width:6%' }
    end
    
    f.inputs "Images", for: [:pic, f.object.pic || Pic.new] do |s|
      s.input :image, label: 'Иллюстрация', as: :file,
        hint:  s.object.image.blank? ? content_tag(:p, "") : image_tag(s.object.image.url)
      s.input :image_cache, as: :hidden 
      s.input :remote_image_url, label: "или вставьте адрес иллюстрации"
      # s.actions
    end
    
    f.actions
  end

  controller do
    before_action :combine_datetime, only: [:create, :update]
 
    def combine_datetime
      params[:event][:start] += ' ' + params[:event][:start_time]
    end
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

end
