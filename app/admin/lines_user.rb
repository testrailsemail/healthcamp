ActiveAdmin.register Lines::User do
  menu priority: 3, label: "Редакторы"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :id, :email, :password, :password_confirmation

  index do
    selectable_column #batch actions checkboxes column
    column :id
    column :email
    column :created_at
    column :updated_at

    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :created_at
      row :updated_at
      
    end
    active_admin_comments
  end

  form do |f|
    f.inputs 'User details' do
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    
    f.actions
  end

  member_action :create, :method => :post do
    @user = Lines::User.new(permitted_params[:user])
    if @user.save
      redirect_to admin_lines_users_path, :notice => "User created successfully."
    else
      redirect_to admin_lines_users_path, :notice => "Error creating user. Try again"
    end
      
  end

end
