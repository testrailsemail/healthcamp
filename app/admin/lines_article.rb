ActiveAdmin.register Lines::Article do
  menu priority: 5, label: "Статьи"
  permit_params :id,:title, :sub_title, :content, :published,
                :hero_image, :slug, :gplus_url, :featured, :document, :teaser
  
  config.clear_action_items!
  action_item :new do
    link_to 'Написать статью', lines.new_admin_article_path
  end
  action_item :edit, only: :show do
    link_to 'Отредактировать в Сатьях', lines.edit_admin_article_path(lines_article)
  end
  action_item :edit, only: :show do
    link_to 'Отредактировать в тут(!)', edit_admin_lines_article_path(lines_article)
  end
  action_item :show, only: [:edit, :show] do
    link_to 'Посмотреть в Сатьях', lines.article_path(lines_article)
  end

  # batch_action :publish do |ids|
  #   Lines::Article.find(ids).each do |article|
  #     redirect_to admin_article_toggle_publish_path(article) and return
  #   end
  #   redirect_to collection_path, alert: "Готово"
  # end
   

  scope "Все", :all, default: true
  scope "Опубликованы", :published

  scope "Посты", :blog do |members|
    members.where(oftype: "blog")
  end
  scope "Опубликованные посты", :blog do |members|
    members.where(oftype: "blog").published
  end
  scope "Статьи", :pages do |members|
    members.where(oftype: "pages")
  end
  scope "Опубликованные статьи", :blog do |members|
    members.where(oftype: "pages").published
  end

  index do
    selectable_column #batch actions checkboxes column
    actions
    column "Type", :oftype
    column :title
    column :sub_title
    column :published
    column :published_at
    column :slug
    column :featured
    column :document
        
  end

  form do |f|
    f.inputs '..................................................................................!Только чтобы подправить текстовую часть статьи!..................................................................................' do
      f.input :oftype, label: "Type", :as => :select, :collection => Lines::Article.uniq.pluck(:oftype)
      f.input :title
      f.input :sub_title
      f.input :teaser, input_html: { rows: 2 }
      f.input :content
      f.input :published
      f.input :slug
      f.input :gplus_url
      
      
    end
    
    f.actions
  end

  member_action :update, :method => :patch do
    @article = Lines::Article.friendly.find(params[:id])
    if @article.update_attributes(permitted_params[:article])
      redirect_to admin_lines_articles_path, :notice => "Article updated successfully."
    else
      render "edit"
    end
      
  end

  member_action :blog, :method => :get do
    @article = Lines::Article.friendly.find(params[:id])
    if @article.update_attributes(oftype: "blog")
      redirect_to request.referrer, :notice => "Article converted to Blog post."
    end
  end

  member_action :pages, :method => :get do
    @article = Lines::Article.friendly.find(params[:id])
    if @article.update_attributes(oftype: "pages")
      redirect_to request.referrer, :notice => "Blog post converted to Article."
    end
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
