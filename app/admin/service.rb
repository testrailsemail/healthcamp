ActiveAdmin.register Service do
  menu priority: 7, label: "Услуги"

  permit_params :name, :description, :price, :body,
    pic_attributes: [:id, :name, :image, :service_id, :remote_image_url]

  action_item :show, only: [:edit, :show] do
    link_to 'Посмотреть на сайте', service_path(service)
  end

  action_item :new, only: :show do
    link_to 'Новая услуга', new_admin_service_path
  end
  # action_item :edit, only: :show do
  #   link_to 'Отредактировать', edit_admin_service_path(service)
  # end
  index do
    selectable_column #batch actions checkboxes column
    actions
    column :id
    column "Название", :name
    column "Описание",:description
    column "Цена",:price
    column "Обновлено",:updated_at
    column "Создано",:created_at
  end
  show do
    attributes_table do
      row ("Название") { service.name }
      row ("Описание") { service.description }
      row ("Цена") { number_to_currency(service.price, locale: :uk) }
      row ("Создана") { service.created_at }
      row ("Иллюстрация") { image_tag service.pic.image.url if service.pic }
    end
    active_admin_comments
  end

  form do |f|
    f.inputs 'Details' do
      f.input :name, label: 'Название'
      f.input :description, label: 'Описание', input_html: { rows: 3 }
      f.input :body, label: 'Тело', input_html: { rows: 7 }
      f.input :price, label: 'Цена'
    end
    
    f.inputs "Images", for: [:pic, f.object.pic || Pic.new] do |s|
      s.input :image, label: 'Иллюстрация', as: :file,
        hint:  s.object.image.blank? ? content_tag(:p, "") : image_tag(s.object.image.url)
      s.input :image_cache, as: :hidden 
      s.input :remote_image_url, label: "или вставьте адрес иллюстрации"
          
      # s.actions
    end
    
    f.actions
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  # controller do
  #   def permitted_params
  #     params.permit!
  #   end
  # end

end
# ActiveAdmin.register Pic do
#   belongs_to :service
#   navigation_menu :service
# end