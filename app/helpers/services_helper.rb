module ServicesHelper
  
  require 'redcarpet'
  require 'pygments'
    # Highlights and formats code fragments with Pygments 
  class HTMLwithPygments < Redcarpet::Render::XHTML
    def block_code(code, language)
      sha = Digest::SHA1.hexdigest(code)
      Rails.cache.fetch ["code", language, sha].join('-') do
        begin
          Pygments.highlight(code, lexer: language)
        rescue => e
          "<div class=\"highlight\"><pre>#{code}</pre></div>"
        end
      end
    end

    def image(link, title, content)
        # preview = link.to_s.split('/')[0..-2].join('/') + '/' + "small_" + link.split('/')[-1] unless link.nil?
        # alttext = "" if link == nil
        # "<a class=\"#{content}\" href=\"#\" rel=\"#{link}\"><img src=\"#{preview}\" class: \"#{content}\" alt=\"#{alttext}\" /></a>"
        link = nil
        "<div class=\"hidden\"></div>"
    end
    # image(link, title, alt_text)
    # def list(contents, list_type)
    #   contents
    # end

    # def list_item(text, list_type)
    #   text
    # end
  end  

  def statick_markdown(text)
      renderer = HTMLwithPygments.new(hard_wrap: true, filter_html: false, with_toc_data: false )
      options = {
        autolink: true,
        no_intra_emphasis: true,
        fenced_code_blocks: true,
        lax_html_blocks: true,
        tables: true,
        strikethrough: true,
        superscript: true,
        xhtml: true,
        no_images:true
      }
      Redcarpet::Markdown.new(renderer, options).render(text).html_safe
  end

end
