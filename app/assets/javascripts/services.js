$( document ).on('turbolinks:load', function() {
 
// $(document).ready(function(){

    $('.fotorama').fotorama();

    if (Modernizr.touch) {
        // show the close overlay button
        // $(".close-overlay").removeClass("hidden");
        $(".overlay-close").removeClass("hidden");
        
        // handle the adding of hover class when clicked
        $(".box").click(function(e){
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".overlay-close").click(function(e){
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".box").hasClass("hover")) {
                $(this).closest(".box").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".box").mouseenter(function(){
            $(this).addClass("hover");
        })
        // handle the mouseleave functionality
        .mouseleave(function(){
            $(this).removeClass("hover");
        });
    }
});