class Service < ApplicationRecord
  has_one :pic,  dependent: :destroy

  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  validates :name, :description, :price, presence: true

  accepts_nested_attributes_for :pic
end
