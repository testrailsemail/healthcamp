class Event < ApplicationRecord
  has_one :pic,  dependent: :destroy

  validates :name, :description, presence: true
  validate :date_cannot_be_in_the_past

  

  accepts_nested_attributes_for :pic

  def date_cannot_be_in_the_past
    if start.present? && start < Date.today
      errors.add(:start, "can't be in the past")
    end
  end
end
