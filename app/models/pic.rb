class Pic < ApplicationRecord
  # Associations
  belongs_to :event, touch: true
  belongs_to :service, touch: true

  # Mount carrierwave picture uploader
  mount_uploader :image, PicUploader

  # Callbacks
  before_create :default_name

  # Returns the default name fo a picture
  def default_name
    self.name ||= File.basename(image.filename, '.*').titleize if image
  end

end
# Pic.new(:remote_image_url=>"http://healthcamp.com.ua/images/demo/320x210_1.png").save