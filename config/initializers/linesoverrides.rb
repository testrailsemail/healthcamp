Lines::PictureUploader.class_eval do
  
  include CarrierWave::Uploader::Configuration
  include CarrierWave::Utilities::Uri
  storage :fog

  version :small, from_version: :preview do
    resize_to_limit(200, 200)
  end
      
end

Lines::HeroImageUploader.class_eval do
  extend ActiveSupport::Concern
  include CarrierWave::Uploader::Configuration
  include CarrierWave::Utilities::Uri
  storage :fog
    
end

Lines::DocumentUploader.class_eval do
  extend ActiveSupport::Concern
  include CarrierWave::Uploader::Configuration
  include CarrierWave::Utilities::Uri
  storage :fog
    
end

Lines::ApplicationController.class_eval do
  extend ActiveSupport::Concern

  private

    def authorize
      session[:return_to] = request.fullpath || root_path
      
      redirect_to login_url, notice: t('lines.please_login') if current_lines_user.nil?
    end

end

Lines::Admin::SessionsController.class_eval do
  extend ActiveSupport::Concern

  def create
    user = Lines::User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to(session[:return_to] || root_path)
      session.delete(:return_to)
    else
      flash.now[:error] = t('lines.login_error')
      render "new"
    end
  end
end

Lines::ArticlesController.class_eval do
  # extend ActiveSupport::Concern
  layout 'lines/application'

  KEYWORDS = CONFIG[:keywords]
  SITE_TITLE = CONFIG[:title]

  # Lists all published articles.
  # Responds to html and atom
  def index
    respond_to do |format|
      format.html {
        @first_page = (params[:page] and params[:page].to_i > 0) ? false : true
        if params[:tag]
          @articles = Lines::Article.where(oftype: "pages").published.tagged_with(params[:tag]).page(params[:page].to_i)
        else
          @articles = Lines::Article.where(oftype: "pages").published.page(params[:page].to_i).padding(1)
        end
        
        if @articles.first_page?
          if @first_article = Lines::Article.where(oftype: "pages").published.first
            @first_article.teaser = nil unless @first_article.teaser.present?
          end
        end
        
        set_meta_tags title: SITE_TITLE,
                      description: CONFIG[:meta_description],
                      keywords: KEYWORDS,
                      open_graph: { title: SITE_TITLE,
                                      type: 'website',
                                      url: articles_url,
                                      site_name: SITE_TITLE,
                                      image: CONFIG[:og_logo]
                                    }

      }
      format.atom{
        @articles = Lines::Article.where(oftype: "pages").published
      }
    end
  end

  def show
      @first_page = true
      @article = Lines::Article.where(oftype: "pages").published.find(params[:id])
      @article.teaser = nil unless @article.teaser.present?
      meta_tags = { title: @article.title,
        type: 'article',
        url: url_for(@article),
        site_name: SITE_TITLE,
      }
      meta_tags[:image] = CONFIG[:host] + @article.image_url if @article.image_url.present?
      set_meta_tags title: @article.title,
                    keywords: KEYWORDS + @article.tag_list.to_s,
                    open_graph: meta_tags
      if request.path != article_path(@article)
        return redirect_to @article, status: :moved_permanently
      end

      @related_articles = Lines::Article.where(oftype: "pages").published.where('id != ?', @article.id).order('').limit(2)
    end
end
