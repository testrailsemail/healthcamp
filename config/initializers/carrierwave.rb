# config/initializers/carrierwave.rb

CarrierWave.configure do |config|
  config.fog_provider = 'fog/aws'
  # config.permissions = 0666
  config.fog_credentials = {
    :provider               => 'AWS',
    region: "eu-west-1",
    :aws_access_key_id      => ENV["AWS_ACCESS_KEY"],
    :aws_secret_access_key  => ENV["AWS_SECRET_KEY"]
  }
  config.fog_directory  = ENV["AWS_BUCKET"]
  config.storage = :fog

  # config.aws_credentials = {
  #   # :provider               => 'AWS',
  #   region: "eu-west-1",                        # required
  #   :access_key_id      => ENV["AWS_ACCESS_KEY"],                        # required
  #   :secret_access_key  => ENV["AWS_SECRET_KEY"]                         # required
  # }
  # config.aws_bucket  = ENV["AWS_BUCKET"]                     # required
  # config.storage = :aws
end

if Rails.env.test? || Rails.env.cucumber?
  CarrierWave.configure do |config|
    config.storage = :file
    config.enable_processing = false
  end

  PicUploader

  CarrierWave::Uploader::Base.descendants.each do |klass|
    next if klass.anonymous?
    klass.class_eval do
      def cache_dir
        "#{Rails.root}/spec/support/uploads/tmp"
      end

      storage :file

      def store_dir
        "#{Rails.root}/spec/support/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
      end
    end
  end

end