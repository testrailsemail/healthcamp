Rails.application.routes.draw do
  
  
  mount Lines::Engine => "/pages"#, as: :blog
  # Lines::Engine.routes.append do
  #   resources :articles, path: '', only: [:index, :show] do
  #     get 'page/:page', action: :index, on: :collection
  #   end
  # end
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get  'project', to: 'static_pages#project'
  root 'events#index' 

  resources :blogs, only: [:show, :index], as: :articles, path: "blog" do
    get :to_blog, :on => :member
    get :to_pages, :on => :member
  end
  resources :services, only: [:show, :index]
  resources :events, only: [:show, :index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
