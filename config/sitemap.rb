# Set the host name for URL creation

# SitemapGenerator::Sitemap.default_host = "http://www.healthcamp.com.ua/"
# SitemapGenerator::Sitemap.sitemaps_host = "http://healthcamp.s3-eu-west-1.amazonaws.com/"
# SitemapGenerator::Sitemap.public_path = 'tmp/'
# SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
# # SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new
# SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new(fog_provider: 'AWS',
#                                          aws_access_key_id: ENV["AWS_ACCESS_KEY"],
#                                          aws_secret_access_key: ENV["AWS_SECRET_KEY"],
#                                          fog_directory: ENV["AWS_BUCKET"],
#                                          fog_region: "eu-west-1")

SitemapGenerator::Sitemap.default_host = "http://www.healthcamp.com.ua/"
SitemapGenerator::Sitemap.sitemaps_host = "http://healthcamp.s3-eu-west-1.amazonaws.com/"
SitemapGenerator::Sitemap.public_path = 'tmp/'
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new(provider: 'AWS',
                                         aws_access_key_id: ENV["AWS_ACCESS_KEY"],
                                         aws_secret_access_key: ENV["AWS_SECRET_KEY"],
                                         fog_directory: ENV["AWS_BUCKET"],
                                         fog_region: "eu-west-1",
                                         permissions: 0666,
                                         storage: :fog)
# SitemapGenerator::Sitemap.adapter = SitemapGenerator::WaveAdapter.new(provider: 'AWS',access_key_id: ENV["AWS_ACCESS_KEY"],
#                                          secret_access_key: ENV["AWS_SECRET_KEY"],
#                                          aws_bucket: ENV["AWS_BUCKET"],
#                                          region: "eu-west-1",
#                                          permissions: 0666,
#                                          storage: :aws)
SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
  add '/project', :priority => 0.85, :changefreq => 'daily'
  add Lines::Engine.routes.url_helpers.root_path, :priority => 0.85
  Lines::Article.where(oftype: "pages").published.find_each do |article|
    add Lines::Engine.routes.url_helpers.article_path(article), :lastmod => article.updated_at
  end
  add articles_path, :priority => 0.85
  Lines::Article.where(oftype: "blog").published.find_each do |article|
    add article_path(article), :lastmod => article.updated_at
  end
  add services_path, :priority => 0.5
  Service.find_each do |service|
    add service_path(service), :lastmod => service.updated_at
  end

end
